import logging as log


UPLOAD_FOLDER = 'store/'

EXTENSIONS = {"TEXT": ["txt", "md"],
              "DOCUMENT": ["rtf", "odf", "ods", "doc",  "docx", "xls", "xlsx"],
              "IMAGE": ["jpg", "jpeg", "png", "gif", "svg", "bmp", "webp"],
              "AUDIO": ["wav", "mp3", "aac", "ogg", "oga", "flac"],
              "DATA": ["csv", "ini", "json", "plist", "xml", "yaml", "yml"],
              "SCRIPT": ["js", "php", "pl", "py", "rb", "sh"],
              "ARCHIVE": ["gz", "bz2", "zip", "tar", "tgz", "txz", "7z"]}

ALLOWED_EXTENSIONS = (EXTENSIONS["TEXT"]
                      + EXTENSIONS["DOCUMENT"]
                      + EXTENSIONS["IMAGE"]
                      + EXTENSIONS["AUDIO"]
                      + EXTENSIONS["DATA"]
                      + EXTENSIONS["ARCHIVE"])


def allowed_file(filename):
    """Check file extension in allowed."""
    return ('.' in filename and filename.rsplit('.', 1)[1].lower()
            in ALLOWED_EXTENSIONS)


# Logger config
log.basicConfig(filename='_log.txt',
                level=log.DEBUG,
                format=u'%(filename)s  [LINE: %(lineno)d]#  '
                       u'%(levelname)s  [%(asctime)s]  %(message)s')
