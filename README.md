doctor_web_task_test
---
Description:
* Server for file storage.

API endpoints:
* `/upload` -- upload file
* `/upload/<file_hash>` -- download file
* `/delete/<file_hash>` -- delete file

Start:
* `sudo apt-get install python3.7`
* `pip install -r requirements.txt`
