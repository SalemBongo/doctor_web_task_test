from os import path, mkdir, walk, remove
from werkzeug.utils import secure_filename
from hashlib import sha256
from flask import (Flask, Response, request, render_template,
                   send_from_directory)

from config import UPLOAD_FOLDER, allowed_file, log


app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


@app.route('/upload/', methods=['GET', 'POST'])
def upload_file():
    """
    Upload file into storage directory and convert filename to hash-string.
    """
    if request.method == 'POST':
        if 'file' not in request.files:
            return Response('No file part', status=500)
        file = request.files['file']
        if file.filename == '':
            return Response('No selected file', status=500)
        if file and allowed_file(file.filename):
            filename = sha256(f'{file}'.encode('ascii')).hexdigest()
            try:
                mkdir(path.join(app.config['UPLOAD_FOLDER'], filename[:2]))
            except OSError as e:
                log.info(e)
                pass
            sub_directory = app.config['UPLOAD_FOLDER'] + filename[:2]
            file.save(path.join(sub_directory, filename))
            return Response('File uploaded successfully', status=200)
    return render_template('upload.html')


@app.route('/upload/<string:file_hash>', methods=['GET', 'POST'])
def download_file(file_hash):
    """Search file by hash-string and send for download."""
    try:
        for root, dirs, files in walk(app.config['UPLOAD_FOLDER']):
            if file_hash in files:
                return send_from_directory(root, file_hash, as_attachment=True)
    except Exception as e:
        log.info(e)
        return Response('File not found', status=404)


@app.route('/delete/<string:file_hash>/', methods=['GET', 'DELETE'])
def delete_file(file_hash):
    """Search file by hash-string and delete."""
    try:
        for root, dirs, files in walk(app.config['UPLOAD_FOLDER']):
            if file_hash in files:
                remove(path.join(root, file_hash))
                return Response('File deleted successfully', status=200)
    except Exception as e:
        log.info(e)
        return Response('File not found', status=404)


if __name__ == '__main__':
    app.run(host='0.0.0.0',
            debug=True,
            threaded=True)
